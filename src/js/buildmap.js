import * as svgUtil from "./util/svg.js";
import * as colourUtil from "./util/colour";
import * as geojsonUtil from "./util/geojson.js";
import * as crossbrowser from "./util/crossbrowser.js";
import * as tooltipUtil from "./util/tooltip.js";
import defaultConfig from "../json/config.default.json";
import world from "../json/world.json";

/* Internal Globals. */
var zoomFactor = 1.0;
var zoomMin = 1.0;
var zoomMax = 2.0;
var translate = [ 0, 0 ];
var svg = null;
var config = null;

/* *** Utility functions *** */
function updateTransform( obj, translate, scale ) {
	obj.setAttribute( "transform", "scale( 1, -1 ) scale( " + scale + ") " + "translate( " + vectorToString( translate ) + " )"  );
}

function copyKeys( dst, src ) {
	for ( var property in src ) {
		if ( src.hasOwnProperty( property ) ) {
			if ( typeof src[ property ] === "object" && !!src[ property ] ) {
				dst[ property ] = dst[ property ] || {};
				copyKeys( dst[ property ], src[ property ] );
			}
			else {
				dst[ property ] = src[ property ];
			}
		}
	}
}
function getDataParameters( data ) {
	/* Holds meta data. */
	var params = {
		min: Infinity,
		max: -Infinity
	};

	/* Calculate minimum and maximum value of the "value" key. */
	for ( var dataKey in data ) {
		if ( data.hasOwnProperty( dataKey ) ) {
			if ( data[ dataKey ].value < params.min ) {
				params.min = data[ dataKey ].value;
			}
			if ( data[ dataKey ].value > params.max ) {
				params.max = data[ dataKey ].value;
			}
		}
	}
	return params;
}

function vectorMagnitude( vecA, $vecB ) {
	var vecB = $vecB || [ 0, 0 ];
	return Math.sqrt( Math.pow( vecB[ 0 ] - vecA[ 0 ], 2 ) + Math.pow( vecB[ 1 ] - vecA[ 1 ], 2 ) );
}

function vectorToString( point ) {
	return "" + point[ 0 ] + ", " + point[ 1 ];
}

/* “Only a Sith deals in absolutes.” - Frank Sinatra */ 
function absoluteToLocalSVG( point, svg ) {
	var absolutePoint = svg.createSVGPoint();
	absolutePoint.x = point[ 0 ];
	absolutePoint.y = point[ 1 ];
	var localPoint = absolutePoint.matrixTransform( svg.getScreenCTM().inverse() );
	return [ localPoint.x, localPoint.y ];
}

/* ************************* */

/* *** Map user events *** */
function mouseMove( event ) {
	var svg = event.currentTarget;
	var currentPosition = absoluteToLocalSVG( [ event.clientX, event.clientY ], svg );
	var lastPosition = absoluteToLocalSVG( [ event.clientX - event.movementX, event.clientY - event.movementY ], svg );
	var deltaPos = [
		currentPosition[ 0 ] - lastPosition[ 0 ],
		currentPosition[ 1 ] - lastPosition[ 1 ]
	];
	translate[ 0 ] += deltaPos[ 0 ];
	translate[ 1 ] -= deltaPos[ 1 ];

	var svgBounds = svg.firstChild.getBBox();
	var extraWidth = zoomFactor * svgBounds.width - svgBounds.width;
	var extraHeight = zoomFactor * svgBounds.height - svgBounds.height;
	var leftTranslateBound = extraWidth / ( 2 * zoomFactor );
	var rightTranslateBound = -extraWidth / ( 2 * zoomFactor );
	var topTranslateBound = -extraHeight / ( 1.625 * zoomFactor );
	var bottomTranslateBound = extraHeight / ( 2.375 * zoomFactor );
	if ( translate[ 0 ] < rightTranslateBound ) {
		translate[ 0 ] = rightTranslateBound;
	}
	if ( translate[ 0 ] > leftTranslateBound ) {
		translate[ 0 ] = leftTranslateBound;
	}
	if ( translate[ 1 ] < topTranslateBound ) {
		translate[ 1 ] = topTranslateBound;
	}
	if ( translate[ 1 ] > bottomTranslateBound ) {
		translate[ 1 ] = bottomTranslateBound;
	}
	updateTransform( svg.firstChild, translate, zoomFactor );
}

function panMapBeginDrag( event ) {
	var svg = event.currentTarget;
	svg.addEventListener( "mousemove", mouseMove );
}

function panMapEndDrag( event ) {
	svg.removeEventListener( "mousemove", mouseMove );
}

function zoomMap( event ) {

	/* Prevent event propagating as a regular scroll on page. */
	event.preventDefault();

	var svg = event.currentTarget;
	var relativeCoords = absoluteToLocalSVG( [ event.clientX, event.clientY ], svg );
	var deltaZoom = event.deltaY * -0.03;
	/* Fix value between minimum and maximum. No need to translate if 
	 * at max or min, return. */

	/* Returns the unit vector of a given 3D vector. */

	zoomFactor += deltaZoom;

	if ( zoomFactor > zoomMax ) {
		zoomFactor = zoomMax;
		updateTransform( svg.firstChild, translate, zoomFactor );
		return;
	}
	if ( zoomFactor < zoomMin ) {
		zoomFactor = zoomMin;
		updateTransform( svg.firstChild, translate, zoomFactor );
		return;
	}

	var translateVector = [ 0, 0];
	if( deltaZoom > 0 && vectorMagnitude( translate, relativeCoords ) > 75 ) {
		translateVector[ 0 ] = translate[ 0 ] - relativeCoords[ 0 ];
		translateVector[ 1 ] = relativeCoords[ 1 ] - translate[ 1 ];
	}
	else if ( deltaZoom < 0 ) {
		var mag = vectorMagnitude( [ -translate[ 0 ], -translate[ 1 ] ] );
		var mult = mag < 10 ? mag / 50 : 1;
		translateVector[ 0 ] = -translate[ 0 ] * mult;
		translateVector[ 1 ] = -translate[ 1 ] * mult;
	}
	var magnitude = vectorMagnitude( translateVector );
	magnitude = magnitude !== 0 ? magnitude : 1;

	var unitVector = [
		translateVector[ 0 ] / magnitude,
		translateVector[ 1 ] / magnitude
	];
	translate = [
		translate[ 0 ] + unitVector[ 0 ] * Math.abs( deltaZoom ) * 100,
		translate[ 1 ] + unitVector[ 1 ] * Math.abs( deltaZoom ) * 100
	];
	updateTransform( svg.firstChild, translate, zoomFactor );
}

function highlightFeature( event ) {
	var featureGroup = event.target.parentElement;
	switch( event.type ) {
		case "mouseover":
			featureGroup.setAttribute( "opacity", "0.8" );
			break;
		case "mouseout":
			featureGroup.setAttribute( "opacity", "1.0" );
			break;
	}
}

function clickWrapper( event ) {
	var ISO_A2 = event.target.parentElement.id;
	config.clickHandler( ISO_A2 );
}

/* ************************* */


/* config
 * ┗━━━━ data <object>
 * ┗━━━━ colourMin <string> 
 * ┗━━━━ colourMax <string> 
 * ┗━━━━ colourBlank <string> 
 * ┗━━━━ colourBorder <string> 
 * ┗━━━━ tooltip <object>
 * 	┗━━━━ type <string>
 * 	┗━━━━ callback <function> */

export function buildMap( usrConfig ) {

	/* Provide sensible defaults for non-critical config options. */
	config = defaultConfig;
	if ( usrConfig !== undefined ) {
		copyKeys( config, usrConfig );
	}
	var svgContainer = document.createElement( "div" );
	svgContainer.setAttribute( "class", "jchoropleth-container" );

	svg = svgUtil.createSVGElement( "svg" );
	svg.style.width = "100%";
	svg.style.height = "auto";
	svgContainer.appendChild( svg );
	crossbrowser.attachEvent( svg, "wheel", zoomMap ); 
	crossbrowser.attachEvent( svg, "mousedown", panMapBeginDrag ); 
	crossbrowser.attachEvent( svg, "mouseover", highlightFeature ); 
	crossbrowser.attachEvent( svg, "mouseout", highlightFeature );
	if ( config.clickHandler !== undefined ) crossbrowser.attachEvent( svg, "click", clickWrapper );
	crossbrowser.attachEvent( document.documentElement, "mouseup", panMapEndDrag ); 
	crossbrowser.attachEvent( svg, "wheel", zoomMap ); 

	/* Create container for tooltip if callback exists in config. */
	if (
		config.tooltip !== undefined &&
		config.tooltip !== null &&
		config.tooltip.callback !== undefined
	) {
		var tooltip = tooltipUtil.createTooltipForMap( config.tooltip.type, svg, config.tooltip.callback, config.data );
		svgContainer.appendChild( tooltip );
	}

	/* Map coordinates are specified in standard cartesian space but
	 * SVGs are drawn with increasing Y values towards the bottom of
	 * the document. This can be rectified by grouping the contents 
	 * and applying a negative scale operation on the group's Y values. */
	var root = svgUtil.createSVGElement( "g" );
	root.setAttribute( "id", "map" );
	svg.appendChild( root );
	root.setAttribute( "transform", "scale( 1, -1 )" );

	var featureList = world.features;
	var dataParams = getDataParameters( config.data );
	for ( var i = 0; i < featureList.length; i++ ) {
		var matchingData = config.data[ featureList[ i ].properties.ISO_A2 ];
		var colour;
		if(
			matchingData !== undefined && 
			matchingData[ "value" ] !== undefined &&
			dataParams.min !== Infinity
		) {
			colour = colourUtil.interpolateColour(
				config.colourMin,
				config.colourMax,
				matchingData[ "value" ],
				dataParams
			);
		} else {
			colour = config.colourBlank;
		}

		var mapFeature = geojsonUtil.parseFeature( featureList[ i ], { 
			"fill": colour,
			"stroke": config.colourBorder,
			"stroke-width": 0.15
		} );

		root.appendChild( mapFeature );
	}

	return svgContainer;
}
