import { buildMap } from "./buildmap.js";

export function createChoropleth( container, config ) {
	var svgContainer = buildMap( config );
	container.appendChild( svgContainer );
	var svg = svgContainer.getElementsByTagName( "svg" )[ 0 ];
	var bounds = svg.getBBox();
	svg.setAttribute( "viewBox", bounds.x + " " + bounds.y + " " + bounds.width + " " + bounds.height );
}

/* jQuery is available, provide jQuery style function call. */
if ( window.jQuery !== undefined ) {
	( function ( $ ) {
		$.fn.jchoropleth = function( config ) {
			createChoropleth( this[ 0 ], config );
			return this;
		};
	}( window.jQuery ));
}
