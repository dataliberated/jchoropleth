/* Tiny-winy factory function to keep things a little bit 
 * neater down the line. */
export function createSVGElement( type ) {
	return document.createElementNS( "http://www.w3.org/2000/svg", type );
}
