import * as crossbrowser from "./crossbrowser.js";


export function createTooltipForMap( type, map, callback, data ) {
	var tooltip = document.createElement( "div" );
	crossbrowser.addClass( tooltip, "jchoropleth-tooltip-" + type );


	function dynamicTooltipMouseMove( evnt ) {
		tooltip.style.left = "" + ( evnt.clientX + 10 ) + "px";
		tooltip.style.top = "" + ( evnt.clientY + 10 ) + "px";
	}
	function dynamicTooltipMouseOver( evnt ) {
		var targetGroup = evnt.target.parentElement;
		if ( evnt.target.tagName.toLowerCase() === "path" && data[ targetGroup.id ] !== undefined ) {
			callback( tooltip, evnt.target.id, data[ targetGroup.id ].data );
			tooltip.style.display = "block"; 
			crossbrowser.attachEvent( this, "mousemove", dynamicTooltipMouseMove );
		}
	}
	function dynamicTooltipMouseOut( evnt ) { 
		if ( evnt.target.tagName.toLowerCase() === "path" ) {
			crossbrowser.detachEvent( this, "mouseover", dynamicTooltipMouseMove );
			tooltip.style.display = "none"; 
			while ( tooltip.firstChild ) {
				tooltip.removeChild( tooltip.firstChild );
			}
		}
	}


	function staticTooltipMouseOver( evnt ) {
		var targetGroup = evnt.target.parentElement;
		if ( evnt.target.tagName.toLowerCase() === "path" && data[ targetGroup.id ] !== undefined ) {
			callback( tooltip, evnt.target.id, data[ targetGroup.id ].data );
			tooltip.style.visibility = "visible";
		}
	}
	function staticTooltipMouseOut( evnt ) {
		if ( evnt.target.tagName.toLowerCase() === "path" ) {
			tooltip.style.visibility = "hidden";
			while ( tooltip.firstChild ) {
				tooltip.removeChild( tooltip.firstChild );
			}
		}
	}

	switch( type.toLowerCase() ) {
		case "static":
			tooltip.style.visibility = "hidden";
			crossbrowser.attachEvent( map, "mouseover", staticTooltipMouseOver );
			crossbrowser.attachEvent( map, "mouseout", staticTooltipMouseOut );
			break;
		case "dynamic":
			tooltip.style.display = "none";
			tooltip.style.position = "fixed";
			crossbrowser.attachEvent( map, "mouseover", dynamicTooltipMouseOver );
			crossbrowser.attachEvent( map, "mouseout", dynamicTooltipMouseOut );
			break;
	}
	return tooltip;
}
