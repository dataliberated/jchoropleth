function standardiseHex( hex ) {
	if ( /^#[A-Fa-f0-9]{3}$/.test( hex ) ) {
		/* String is a valid shortened hex RGB. Expand to full length */
		var chars = hex.split( "" );
		return "#" + chars[ 1 ] + chars[ 1 ] + chars[ 2 ] + chars[ 2 ] + chars[ 3 ] + chars[ 3 ];
	}
	else if( /^#[A-Fa-f0-9]{6}$/.test( hex ) ) {
		/* String is a valid full length hex RGB. */
		return hex;
	} else
	{
		/* String is not a valid hex RGB. */
		throw new Error( "Not a valid hex RGB string." );
	}
}

export function hexToRGBTriplet( hex ) {
	try {
		hex = standardiseHex( hex );
		return [
			parseInt( hex.slice( 1, 3 ), 16 ),
			parseInt( hex.slice( 3, 5 ), 16 ),
			parseInt( hex.slice( 5 ), 16 ),
		];
	} catch( ex ) {
		throw ex;
	}
}

export function RGBTripletToHex( triplet ) {
	return "#" +
		Math.round( triplet[ 0 ] ).toString( 16 ) +
		Math.round( triplet[ 1 ] ).toString( 16 ) +
		Math.round( triplet[ 2 ] ).toString( 16 );
}

export function interpolateColour( colourMin, colourMax, value, metaData ) {
	/* Convert hex colour to RGB triplet. If colour(s) cannot be parsed, 
	 * colourMin will be returned back. */
	try {
		if ( metaData.min === metaData.max ) {
			return colourMax;
		}
		else {
			var min = hexToRGBTriplet( colourMin );
			var max = hexToRGBTriplet( colourMax );
			var diff = [
				max[ 0 ] - min[ 0 ],
				max[ 1 ] - min[ 1 ],
				max[ 2 ] - min[ 2 ]
			];
			var factor = ( value - metaData.min ) / ( metaData.max - metaData.min );
			var resultRGB = [
				min[ 0 ] + factor * diff[ 0 ],
				min[ 1 ] + factor * diff[ 1 ],
				min[ 2 ] + factor * diff[ 2 ]
			];
			return RGBTripletToHex( resultRGB );
		}
	} catch( ex ) {
		return colourMin;
	}
	
}
