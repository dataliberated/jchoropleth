import * as svgUtil from "./svg.js";

/* ( X, Y ) polygon component and movement type to SVG path command. */
function parseCoord( coord, commandType ) {
	var commandString = commandType;
	for ( var i = 0; i < coord.length; i ++ ) {
		commandString += coord[ i ];
		if ( i !== coord.length - 1 ) {
			commandString += ",";
		}
	}
	return commandString;
}

/* GEOJSON polygon to string of SVG path commands. */
function parsePolygon( polygon ) {
	var pathDString = "";
	pathDString += parseCoord( polygon[ 0 ], "M" );
	for( var j = 1; j < polygon.length; j++ )
	{
		pathDString += parseCoord( polygon[ j ], "L" );
	}
	var polygonElement = svgUtil.createSVGElement( "path" );
	polygonElement.setAttribute( "d", pathDString );
	return polygonElement;
}

/* Feature to SVG DOM object. */
export function parseFeature( feature, style ) {
	/* Useful for countries consisting of multiple polygons, 
	* not so much for single polygon countries. */
	var featureContainer = svgUtil.createSVGElement( "g" );
	featureContainer.setAttribute( "id", feature.properties.ISO_A2 );
	for ( var attribute in style ) {
		if ( style.hasOwnProperty( attribute ) ) {
			featureContainer.setAttribute( attribute, style[ attribute ] );
		}
	}
	switch ( feature.geometry.type ) {
		/* Feature consists of a single polygon. */
		case "Polygon":
			featureContainer.appendChild( parsePolygon( feature.geometry.coordinates[ 0 ] ) );
			break;
		/* Feature consists of multiple polygons. */
		case "MultiPolygon":
			for ( var i = 0; i < feature.geometry.coordinates.length; i++ ) {
				featureContainer.appendChild( parsePolygon( feature.geometry.coordinates[ i ] ) );
			}
			break;
	}
	return featureContainer;
}
