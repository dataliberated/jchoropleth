
/* Fallback to IE8(-)'s proprietary event attaching function if
 * standard not available. */
export function attachEvent( element, event, callback ) {
	if ( element.addEventListener !== undefined ) {
		element.addEventListener ( event, callback );
	} else {
		element.attachEvent( event, callback );
	}
}

export function detachEvent( element, event, callback ) {
	if ( element.addEventListener !== undefined ) {
		element.removeEventListener ( event, callback );
	} else {
		element.detachEvent( event, callback );
	}
}


/* Polyfill for adding a class to an element in older browsers. */
export function addClass( element, className ) {
	try {
		element.classList.add( className );
	} catch( ex ) {
		if ( e instanceof TypeError ) {
			element.className = element.className + " " + className;
		}
	}
}
