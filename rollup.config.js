import json from "rollup-plugin-json";
import { terser } from "rollup-plugin-terser";

const developmentBundle = {
	input: "src/js/main.js",
	output: {
		file: "dist/jchoropleth.js",
		format: "iife",
		name: "jchoropleth"
	},
	plugins: [
		json()
	]
};

const productionBundle = {
	input: "src/js/main.js",
	output: {
		file: "dist/jchoropleth.min.js",
		format: "iife",
		name: "jchoropleth"
	},
	plugins: [
		json(),
		terser()
	]
};

export default args => {
	if( args.configBuildDev === true ) {
		return [ developmentBundle, productionBundle ];
	} else {
		return productionBundle;
	}
}
